#!/bin/sh

set -xe

if [ -t 1 ]; then
	version=

	dir="$(dirname "$0")"
	if [ -e "$dir/.git" ]; then
		version="$(git describe)"
	fi
	outfile="wtf${version:+-$version}.tar.gz"
else
	outfile=/dev/stdout
fi

exec tar \
	--owner 0 --group 0 \
	-C root \
	-czf "$outfile" .
